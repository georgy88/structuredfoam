#!/usr/bin/python2.7 python2.7
# -*- coding: utf-8 -*-

# by Georg Babij
__author__ = 'Georg Babij'
__email__ = 'g.babij@gmail.com'
__status__ = 'In production'
import os
import sys
import numpy as np





from globalVariables import *

# Wechsel in constant/polyMesh, um das Gitter einzuladen
print ('Der aktuelle Pfad ist:')
print(os.getcwdu( ) + "\n")
os.chdir( '../constant/polyMesh' )

if '/constant/polyMesh' in os.getcwd( ):
	print('Das Skript wird im richtigen Verzeichnis ausgeführt.')
else:
	sys.exit('Programm wird im falschen Ordner ausgeführt')



from readPoints import *
from readFaces import *
from readBoundary import *
from readOwnersAndNeighbours import *
from sortCells import *
from readPAndU import *
