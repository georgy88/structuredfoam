#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

# by Georg Babij

from readFaces import faces
import numpy as np
import sys
from globalVariables import *
# Read Boundaries
boundaries = [ ]
fileBoundary = open( 'boundary', 'r' )
for line in fileBoundary:
	# print line
	if (line.find( '/*' ) >= 0) \
			or (line.find( '*/' ) >= 0) \
			or (line.find( '//' ) >= 0) \
			or (line.find( '=' ) >= 0) \
			or (line.find( '|' ) >= 0) \
			or (line.find( 'location' ) >= 0) \
			or (line.find( 'version' ) >= 0) \
			or (line.find( 'format' ) >= 0) \
			or (line.find( 'class' ) >= 0) \
			or (line.find( 'FoamFile' ) >= 0) \
			or (line.find( 'points' ) >= 0) \
			or (line.find( '{' ) >= 0) \
			or (line.find( '}' ) >= 0) \
			or (line.find( 'faces' ) >= 0) \
			or (line.find( 'note' ) >= 0) \
			or (line.find( 'object' ) >= 0) \
			or (line.find( 'owner' ) >= 0) \
			or (line == '\n') or (line == '(\n') or (line == ')\n') \
			or (line.find( 'type' ) >= 0) \
			or (line.find( 'inGroups' ) >= 0) \
			or (line.find( 'nFaces') >= 0) \
			or (line.find('startFace') >= 0):
		continue
	else:
		line = line.rstrip( '\n' + '' )
		boundaries.append( line )

fileBoundary.close( )
boundaries = filter( None, boundaries )

boundaries[0]=np.int64(boundaries[0])
for i in np.arange(1,len(boundaries),1):
	boundaries[i]= boundaries[i].strip(' ')

# Überprüfung ob alle Boundaries richtig eingelesen wurden
if int( boundaries [ 0 ] ) == (len( boundaries ) - 1):
	print('\n')
	print ('')
else:
	print('\n')
	print('Es ist ein Fehler beim Einlesen der Nachbarzellen aufgetreten')
	sys.exit('Das Programm wird beendet')
del boundaries[0]

print('\n')
print(boundaries)


#If boundaries are needed to be hardcoded
#boundaries = ['movingWall', 'fixedWalls', 'frontAndBack']
