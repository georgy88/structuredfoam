#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# by Georg Babij


from readPoints import points
import numpy as np
import sys

print('\n')

faces = [ ]
# Einlesen der Faces
filefaces = open( 'faces', 'r' )
for line in filefaces:
	# print line
	if (line.find( '/*' ) >= 0) \
			or (line.find( '*/' ) >= 0) or (line.find( '//' ) >= 0) or (line.find( '=' ) >= 0) \
			or (line.find( '|' ) >= 0) or (line.find( 'location' ) >= 0) or (line.find( 'version' ) >= 0) \
			or (line.find( 'format' ) >= 0) or (line.find( 'class' ) >= 0) or (line.find( 'FoamFile' ) >= 0) \
			or (line.find( 'points' ) >= 0) or (line.find( '{' ) >= 0) or (line.find( '}' ) >= 0) \
			or (line.find( 'faces' ) >= 0) or (line == '\n'):
		continue
	else:
		line = line.rstrip( '\n' + '' )
		if line == '(' or line == ')':
			continue
		else:
			faces.append( line )

filefaces.close( )
faces = filter( None, faces )

# Überprüfe ob alle Faces richtig eingelesen wurden
if int( faces [ 0 ] ) == (len( faces ) - 1):
	print('\n')
	print('Alle Faces wurden erfolgreich eingelesen')
else:
	print('\n')
	print('Es ist ein Fehler beim Einlesen der Faces aufgetreten')

del faces [ 0 ]
del filefaces


print('\n')
print('Output der eingelesenen Faces')
print(faces)

faceslist = faces
del faces

faces = np.zeros( (len( faceslist )), dtype=[ ('index', np.int64), ('pointsIndex1', np.int64), ('pointsIndex2', np.int64),
										('pointsIndex3', np.int64), ('pointsIndex4', np.int64), ('XCoordinate', np.float64),
										('YCoordinate', np.float64), ('ZCoordinate', np.float64), ('OwnerCell', np.int64),
										('NeighbourCell', np.int64) ] )

moreFourFaces = False

for i in range( len( faceslist ) ):
	faces [ i ] [ 'index' ] = np.int64( i )
	zwischenstring = faceslist [ i ]
	if zwischenstring.find( '4(' ) != 0:
		print('Face mit mehr oder weniger als 4 Punkten vorhanden')
		moreFourFaces = True
	else:
		pass
	zwischenstring = zwischenstring.replace( '4(', '' )
	zwischenstring = zwischenstring.replace( ')', '' )
	zwischenstring = zwischenstring.split( )
	faces [ i ] [ 'pointsIndex1' ] = np.int64( zwischenstring [ 0 ] )
	faces [ i ] [ 'pointsIndex2' ] = np.int64( zwischenstring [ 1 ] )
	faces [ i ] [ 'pointsIndex3' ] = np.int64( zwischenstring [ 2 ] )
	faces [ i ] [ 'pointsIndex4' ] = np.int64( zwischenstring [ 3 ] )

if moreFourFaces == True:
	print('Face mit mehr oder weniger als 4 Punkten vorhanden. Programm wird beendet')
	sys.exit('Programm wird beendet')
else:
	pass

for i in range( len( faces ) ):
	# Berechnung der x-Koordinate
	faces [ i ] [ 'XCoordinate' ] = (points [ faces [ i ] [ 'pointsIndex1' ]  ] [ 'XCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex2' ]  ] [ 'XCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex3' ]  ] [ 'XCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex4' ]  ] [ 'XCoordinate' ]) / 4.0

	# Berechnung der y-Koordinate
	faces [ i ] [ 'YCoordinate' ] = (points [ faces [ i ] [ 'pointsIndex1' ]  ] [ 'YCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex2' ]  ] [ 'YCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex3' ]  ] [ 'YCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex4' ]  ] [ 'YCoordinate' ]) / 4.0

	# Berechnung der z-Koordinate
	faces [ i ] [ 'ZCoordinate' ] = (points [ faces [ i ] [ 'pointsIndex1' ]  ] [ 'ZCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex2' ]  ] [ 'ZCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex3' ]  ] [ 'ZCoordinate' ] +
									 points [ faces [ i ] [ 'pointsIndex4' ]  ] [ 'ZCoordinate' ]) / 4.0
