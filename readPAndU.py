#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# by Georg Babij

from sortCells import cellsmatrix as cm
import os
import numpy as np
import string
from pyevtk.hl import gridToVTK
from readPoints import pointsmatrix
from readBoundary import boundaries
from globalVariables import dimensionCells

os.chdir('../..')
# print(os.getcwd() + '\n')
TIMES = os.popen("ls -d [0-9]* ").readlines()

#Sortierung der eingelesenen Zeiten. Wenn nötig verbeserungswürdig
#TIMES.extend(os.popen("ls -d [1-9]* ").readlines())

# zwischen=np.empty(len(TIMES))
#
# for i in range(len(TIMES)):
# 	TIMES[i]=TIMES[i].strip(' ' + '\n' )
# 	zwischen[i]=np.float64(TIMES[i])
#
# SortierungsIndex=np.argsort(zwischen, kind='quicksort')
# # zwischen=np.sort(zwischen, kind='mergesort')
# del zwischen
#
# zwischen=TIMES
# for i in range(len(TIMES)):
# 	TIMES[i] = zwischen[SortierungsIndex[i]]
#
# del zwischen


print ('Der aktuelle Pfad ist:')
print(os.getcwdu( ) + "\n")

if os.path.exists(os.getcwdu() + '/VTS') == False :
	os.mkdir('VTS')
else:
	# os.chdir('VTS')
	# os.remove('*')
	# os.chdir('..')
	# os.removedirs('VTS')
	# os.mkdir('VTS')
	pass

i = 0
for TIME in TIMES:
	if TIME == '0\n':
		continue
	cellsmatrix=cm
	cd_string = TIME.strip(' ' + '\n')
	#cd_string = string.replace(cd_string, '\n', '')
	os.chdir(cd_string)

	# Einlesen der Druckdaten
	filePressure = open('p', 'r')
	p = []
	for line in filePressure:
		for i in range(len(boundaries)):
			if (line.find(boundaries[i]) >= 0):
				line = 'empty'
		if (line.find('/*') >= 0) or (line.find('*/') >= 0) \
				or (line.find('//') >= 0) \
				or (line.find('=') >= 0) \
				or (line.find('|') >= 0) \
				or (line.find('location') >= 0) \
				or (line.find('version') >= 0) \
				or (line.find('format') >= 0) \
				or (line.find('class') >= 0) \
				or (line.find('FoamFile') >= 0) \
				or (line.find('points') >= 0) \
				or (line.find('{') >= 0) \
				or (line.find('}') >= 0) \
				or (line.find('dimensions') >= 0) \
				or (line.find('internalField') >= 0) \
				or (line.find('empty') >= 0) \
				or (line.find('zeroGradient') >= 0) \
				or (line.find('object') >= 0) \
				or (line.find('boundaryField') >= 0) \
				or (line.find(';') >= 0) \
				or (line.find(',') >= 0):
			continue
		else:
			line = line.rstrip('\n' + '')
			if line == '(' or line == ')':
				continue
			else:
				p.append(line)

	filePressure.close()
	p = filter(None, p)

	# Überprüfe ob alle Zellen eingelesen wurden
	if int(p[0]) == (len(p) - 1):
		print('\n')
		print('Alle Zellen wurden erfolgreich eingelesen')
	else:
		print('\n')
		print('Es ist ein Fehler beim Einlesen der Zellen aufgetreten')
	del p[0]

	pList = p
	del p
	del filePressure

	# Einlesen der Druck-Matrix
	p = np.zeros((len(pList)), dtype=[('index', np.int64), ('pressure', np.float64)])

	for i in range(len(pList)):
		p[i]['index'] = np.int64(i)
		zwischenstring = pList[i]
		zwischenstring = zwischenstring.strip(' ')
		p[i]['pressure'] = np.float64(zwischenstring)
		index = np.argwhere(cellsmatrix['index'] == p[i]['index'])
		# index = str(index)
		# index = index.replace('[', ' ')
		# index = index.replace(']', ' ')
		# index = index.strip()
		# index = index.split(' ')
		# index = np.int64(index)
		#cellsmatrix[index[0], index[1], index[2]]['pressure'] = p[i]['pressure']
		index=np.int64(index)
		cellsmatrix[index]['pressure'] = p[i]['pressure']



	del i
	del zwischenstring

	# Einlesen der Strömungsgeschwindigkeitsdaten
	fileVelocity = open('U', 'r')
	U = []
	for line in fileVelocity:
		line.replace(';', ' ')
		line.replace(',', ' ')
		for i in range(len(boundaries)):
			if (line.find(boundaries[i]) >= 0):
				line = 'empty'
		if (line.find('/*') >= 0) \
				or (line.find('*/') >= 0) \
				or (line.find('object') >= 0) \
				or (line.find('//') >= 0) \
				or (line.find('=') >= 0) \
				or (line.find('|') >= 0) \
				or (line.find('location') >= 0) \
				or (line.find('version') >= 0) \
				or (line.find('format') >= 0) \
				or (line.find('class') >= 0) \
				or (line.find('FoamFile') >= 0) \
				or (line.find('points') >= 0) \
				or (line.find('{') >= 0) \
				or (line.find('}') >= 0) \
				or (line.find('dimensions') >= 0) \
				or (line.find('internalField') >= 0) \
				or (line.find('type') >= 0) \
				or (line.find('empty') >= 0) \
				or (line.find('uniform') >= 0) \
				or (line.find('boundaryField') >= 0) \
				or (line.find(',') >= 0) \
				or (line.find(';') >= 0):
			continue
		else:
			line = line.strip('\n' + '')
			if line == '(' or line == ')':
				continue
			else:
				U.append(line)

	fileVelocity.close()
	U = filter(None, U)


	# Überprüfe ob alle Zellen eingelesen wurden
	if int(U[0]) == (len(U) - 1):
		print('\n')
		print('Alle Zellen wurden erfolgreich eingelesen')
	else:
		print('\n')
		print('Es ist ein Fehler beim Einlesen der Zellen aufgetreten')
	del U[0]

	UList = U
	del U
	del fileVelocity

	# Einlesen der Strömungsgeschwindigkeitsmatrix
	U = np.zeros((len(UList)), dtype=[('index', np.int64), ('UVelocity', np.float64), ('VVelocity', np.float64),
										('WVelocity', np.float64)])

	for i in range(len(UList)):
		U[i]['index'] = np.int64(i)
		zwischenstring = UList[i]
		zwischenstring = zwischenstring.replace('(', ' ')
		zwischenstring = zwischenstring.replace(')', ' ')
		zwischenstring = zwischenstring.strip()
		zwischenstring = zwischenstring.split(' ')
		U[i]['UVelocity'] = np.float64(zwischenstring[0])
		U[i]['VVelocity'] = np.float64(zwischenstring[1])
		U[i]['WVelocity'] = np.float64(zwischenstring[2])
		index = np.argwhere(cellsmatrix['index'] == U[i]['index'])
		# index = str(index)
		# index = index.replace('[', ' ')
		# index = index.replace(']', ' ')
		# index = index.strip()
		# index = index.split(' ')
		# index = np.int64(index)
		# cellsmatrix[index[0], index[1], index[2]]['UVelocity'] = U[i]['UVelocity']
		# cellsmatrix[index[0], index[1], index[2]]['VVelocity'] = U[i]['VVelocity']
		# cellsmatrix[index[0], index[1], index[2]]['WVelocity'] = U[i]['WVelocity']
		index = np.int64(index)
		cellsmatrix[index]['UVelocity'] = U[i]['UVelocity']
		cellsmatrix[index]['VVelocity'] = U[i]['VVelocity']
		cellsmatrix[index]['WVelocity'] = U[i]['WVelocity']

	del i
	del zwischenstring
	del index
	x=pointsmatrix['XCoordinate']
	y=pointsmatrix['YCoordinate']
	z=pointsmatrix['ZCoordinate']
	cellsmatrix=np.reshape(cellsmatrix,dimensionCells,order='C')
	gridToVTK('../VTS/structuredGrid--' + cd_string ,x,y,z, cellData = {'pressure' : cellsmatrix [ 'pressure' ],
																'U':cellsmatrix['UVelocity'],
																'V':cellsmatrix['VVelocity'],
																'W':cellsmatrix['WVelocity'],
																'VelocityMagnitude': np.sqrt(np.square(cellsmatrix['UVelocity']) + np.square(cellsmatrix['VVelocity'])
																+ np.square(cellsmatrix['WVelocity'])) } )
	del cellsmatrix
	os.chdir('..')
print( '\n' )
print( cm )
print('\n' + TIME)

print('\n')
print('Ende des Programms')
