#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

# by Georg Babij

import numpy as np
import sys

maxXCells=np.int64(sys.argv[1])
maxYCells=np.int64(sys.argv[2])
maxZCells=np.int64(sys.argv[3])

#maxXCells = 10
# = np.int64( input( "maximale Zellen in x-Koordinatenrichtung: " ) )
#maxYCells = 5
# = np.int64( input( "maximale Zellen in y-Koordinatenrichtung: " ) )
#maxZCells = 1
# = np.int64( input( "maximale Zellen in z-Koordinatenrichtung: " ) )

maxCells = maxXCells * maxYCells * maxZCells
#maxCells = np.max( faces [ : ] [ 'owner' ] ) + 1


maxXPoints = maxXCells + 1
maxYPoints = maxYCells + 1
maxZPoints = maxZCells + 1

dimensionPoints = (maxXPoints, maxYPoints, maxZPoints)
dimensionCells =  (maxXCells, maxYCells, maxZCells)
