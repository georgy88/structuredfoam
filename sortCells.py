#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# by Georg Babij

import numpy as np
from readOwnersAndNeighbours import cells
from readOwnersAndNeighbours import faces
from globalVariables import *


print( '\n' )
print( 'Faces:' )
print( faces )
print( '\n' )
print( 'Cells:' )
print( cells )



#print( '\n' )
#print( np.argsort( cells, order=('XCoordinate') ) )


#cellsmatrix = np.zeros( dimensionCells, dtype=[('index', np.int64), ('XCoordinate', np.float64), ('YCoordinate', np.float64),
#										  ('ZCoordinate', np.float64),('pressure', np.float64),('UVelocity', np.float64),('VVelocity', np.float64),('WVelocity', np.float64)] )



# zwischen = (np.sort( cells, order=('XCoordinate'), kind='mergesort' ))
# print( zwischen )
#
# i = 0
# j = 0
# k = 0
# l = 0
#
# while l < len( cells ):
# 	if j == maxYCells:
# 		j = 0
# 		k += 1
# 		if k == maxZCells:
# 			i += 1
# 			k = 0
# 			j = 0
#
# 	else:
# 		cellsmatrix[[i], [j], [k]] = zwischen[l]
# 		j += 1
# 		l += 1
# else:
# 	pass
#
# 	if int( np.float64( l + 1 ) / maxYCells / maxZCells ) > maxXCells:
# 		print( 'Es ist ein Fehler beim Einlesen Zellenmatrix aufgetreten!' )
#
# del i, j, k, l
# del zwischen
#
# for i in range( maxXCells ):
# 	k = 0
# 	l = 0
# 	zwischen = np.zeros( (maxYCells, maxZCells),
# 						 dtype=[('index', np.int64), ('XCoordinate', np.float64), ('YCoordinate', np.float64),
# 								('ZCoordinate', np.float64)] )
# 	while k < maxYCells:
# 		zwischen[k, l] = cellsmatrix[[i], [k], [l]]
# 		k += 1
# 	else:
# 		k = 0
# 		l += 1
# 		if l == maxZCells:
# 			print( 'Zwischenmatrix wurde erfolgreich eingelesen \n' )
# 		elif l > maxZCells:
# 			print( 'Es ist ein Fehler beim Einlesen der Zwischenmatrix aufgetreten \n' )
# 		else:
# 			pass
#
# 	zwischen = np.sort( zwischen, axis=None, kind='mergesort', order='YCoordinate' )
# 	zwischen.shape=(maxYCells,maxZCells)
#
#
#
# 	zwischen = np.sort( zwischen, kind='mergesort', order='ZCoordinate' )
#
# 	k = 0
# 	l = 0
# 	while k < maxYCells:
# 		cellsmatrix[[i], [k], [l]] = zwischen[k, l]
# 		k += 1
# 	else:
# 		k = 0
# 		l += 1
# 		if l == maxZCells:
# 			print( 'Zwischenmatrix ausgelesen \n' )
# 		elif l > maxZCells:
# 			print( 'Es ist ein Fehler beim Auslesen der Zwischenmatrix aufgetreten \n' )
# 		else:
# 			pass
#
#
#
#
# print( '\n' )
# print('Das ist die ursprüngliche cellsmatrix')
# print( cellsmatrix )

cellsmatrix = np.zeros(maxCells, dtype=[('index', np.int64), ('XCoordinate', np.float64), ('YCoordinate', np.float64), ('ZCoordinate', np.float64),
										('pressure', np.float64),
										('UVelocity', np.float64),('VVelocity', np.float64),('WVelocity', np.float64)] )
cellsmatrix['index'] = cells ['index']
cellsmatrix['XCoordinate'] = cells ['XCoordinate']
cellsmatrix['YCoordinate'] = cells ['YCoordinate']
cellsmatrix['ZCoordinate'] = cells ['ZCoordinate']
#Sortieralgorithmus ohne Beachtung von Maschinenungenauigkeit
#cellsmatrix = (np.sort( cellsmatrix, order=('XCoordinate', 'YCoordinate', 'ZCoordinate'), kind='mergesort' ))

#Sortieralgorithmus mit Beachtung von Maschinenungenauigkeit
cellsmatrix = np.sort( cellsmatrix, order=('ZCoordinate'), kind='mergesort' )
for i in range(maxZCells):
	#zwischen=np.zeros(maxXCells*maxYCells)
	#cellsmatrix[(i)*(maxXCells*maxYCells):(maxXCells*maxYCells-1)+(maxXCells*maxYCells)*(i)] = \
	#np.sort(cellsmatrix[(i)*(maxXCells*maxYCells):(maxXCells*maxYCells-1)+(maxXCells*maxYCells)*(i)],order=( 'YCoordinate', 'XCoordinate'), kind='mergesort' )
	zwischen= \
	np.sort(cellsmatrix[(i)*(maxXCells*maxYCells):(maxXCells*maxYCells)*(i+1)],order=( 'YCoordinate'), kind='mergesort' )
	for j in range(maxYCells):
		zwischen[(j)*(maxXCells):(j+1)*maxXCells] = \
		np.sort(zwischen[(j)*(maxXCells):(j+1)*maxXCells], order=('XCoordinate'), kind='mergesort')
	cellsmatrix[(i)*(maxXCells*maxYCells):(maxXCells*maxYCells)*(i+1)]=zwischen
	print('\n')
	print('zwischen:')
	print(zwischen)
del zwischen

#Dimensionierung der Zellenmatrix erfolgt in ReadPandU
#cellsmatrix=cellsmatrix.reshape(dimensionCells)

print('\n')
print('Das ist die cellsmatrix mit np.sort bearbeitet')
print(cellsmatrix)
