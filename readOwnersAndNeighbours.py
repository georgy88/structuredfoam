#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# by Georg Babij

from readFaces import faces
import numpy as np
import sys
from globalVariables import *
# Read Owners
owner = [ ]
fileowner = open( 'owner', 'r' )
for line in fileowner:
	# print line
	if (line.find( '/*' ) >= 0) \
			or (line.find( '*/' ) >= 0) \
			or (line.find( '//' ) >= 0) \
			or (line.find( '=' ) >= 0) \
			or (line.find( '|' ) >= 0) \
			or (line.find( 'location' ) >= 0) \
			or (line.find( 'version' ) >= 0) \
			or (line.find( 'format' ) >= 0) \
			or (line.find( 'class' ) >= 0) \
			or (line.find( 'FoamFile' ) >= 0) \
			or (line.find( 'points' ) >= 0) \
			or (line.find( '{' ) >= 0) \
			or (line.find( '}' ) >= 0) \
			or (line.find( 'faces' ) >= 0) \
			or (line.find( 'note' ) >= 0) \
			or (line.find( 'object' ) >= 0) \
			or (line.find( 'owner' ) >= 0) \
			or (line == '\n') or (line == '(\n') or (line == ')\n'):
		continue
	else:
		line = line.rstrip( '\n' + '' )
		owner.append( line )

fileowner.close( )
owner = filter( None, owner )

# Überprüfe ob alle Zellen richtig eingelesen wurden
if np.int64( owner [ 0 ] ) == (len( owner ) - 1):
	print('\n')
	print ('Alle Zellen wurden erfolgreich eingelesen')
else:
	print('\n')
	print('Es ist ein Fehler beim Einlesen der Zellen aufgetreten')

del owner [ 0 ]
del fileowner

for i in range( len( owner ) ):
	faces [ i ] [ 'OwnerCell' ] = np.int64( owner [ i ] )

# Read Neighbours
neighbour = [ ]
fileneighbour = open( 'neighbour', 'r' )
for line in fileneighbour:
	# print line
	if (line.find( '/*' ) >= 0) or (line.find( '*/' ) >= 0) or (line.find( '//' ) >= 0) \
			or (line.find( '=' ) >= 0) or (line.find( '|' ) >= 0) or (line.find( 'location' ) >= 0) \
			or (line.find( 'version' ) >= 0) or (line.find( 'format' ) >= 0) or (line.find( 'class' ) >= 0) \
			or (line.find( 'FoamFile' ) >= 0) or (line.find( 'points' ) >= 0) or (line.find( '{' ) >= 0) \
			or (line.find( '}' ) >= 0) or (line.find( 'faces' ) >= 0) or (line.find( 'note' ) >= 0) \
			or (line.find( 'object' ) >= 0) or (line.find( 'owner' ) >= 0) \
			or (line == '\n') or (line == '(\n') or (line == ')\n'):
		continue
	else:
		line = line.rstrip( '\n' + '' )
		neighbour.append( line )

fileneighbour.close( )
neighbour = filter( None, neighbour )

# Überprüfe ob alle Zellen richtig eingelesen wurden
if int( neighbour [ 0 ] ) == (len( neighbour ) - 1):
	print('\n')
	print ('Alle Nachbarzellen wurden erfolgreich eingelesen')
else:
	print('\n')
	print('Es ist ein Fehler beim Einlesen der Nachbarzellen aufgetreten')

NumberOfNeigbourItems = np.int64(neighbour [0])
del neighbour [ 0 ]
del fileneighbour



for i in range( len( neighbour ) ):
	faces [ i ] [ 'NeighbourCell' ] = np.int64( neighbour [ i ] )


cells = np.zeros( maxCells, dtype=[ ('index', np.int64), ('XCoordinate', np.float64), ('YCoordinate', np.float64), ('ZCoordinate', np.float64) ])
#									,('NeighbourWest',np.int64), ('NeighbourNorth',np.int64), ('NeighbourEast',np.int64), ('NeighbourSouth',np.int64)] )


#Suche nach allen Faces, die zu einer Zelle gehören, um daraus den Zellmittelpunkt zu errechnen.
#Abgleich, ob eine Zelle als Neighbour oder Owner bezüglich einer Face eingetragen ist.
for i in range( maxCells ):
	l = 0
	zwischen = np.zeros( (6, 3) )
	for j in range( len( faces ) ):
		#if l==6:
		#	break
		if faces [ j ] [ 'OwnerCell' ] == i:
			zwischen [ l, 0 ] = faces [ j ] [ 'XCoordinate' ]
			zwischen [ l, 1 ] = faces [ j ] [ 'YCoordinate' ]
			zwischen [ l, 2 ] = faces [ j ] [ 'ZCoordinate' ]
			l += 1
		else:
			continue
	while l < 6:
		for k in range( NumberOfNeigbourItems ):
			if faces [ k ] [ 'NeighbourCell' ] == i and faces [ k ] [ 'NeighbourCell' ] != 0:
				zwischen [ l, 0 ] = faces [ k ] [ 'XCoordinate' ]
				zwischen [ l, 1 ] = faces [ k ] [ 'YCoordinate' ]
				zwischen [ l, 2 ] = faces [ k ] [ 'ZCoordinate' ]
				l += 1
			else:
				pass
	else:
		pass
	print(l)
	# print(zwischen)
	cells [ i ] [ 'index' ] = np.int64( i )
	cells [ i ] [ 'XCoordinate' ] = np.sum( zwischen [ :, 0 ] ) / 6.0
	cells [ i ] [ 'YCoordinate' ] = np.sum( zwischen [ :, 1 ] ) / 6.0
	cells [ i ] [ 'ZCoordinate' ] = np.sum( zwischen [ :, 2 ] ) / 6.0

del zwischen
