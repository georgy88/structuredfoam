#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# by Georg Babij

import os
import numpy as np
import scipy
import math
from globalVariables import *





# Einlesen der Punkte
filepoints = open( 'points', 'r' )
points = [ ]
for line in filepoints:
	# print line
	if (line.find( '/*' ) >= 0) \
			or (line.find( '*/' ) >= 0) \
			or (line.find( '//' ) >= 0) \
			or (line.find( '=' ) >= 0) \
			or (line.find( '|' ) >= 0) \
			or (line.find( 'location' ) >= 0) \
			or (line.find( 'version' ) >= 0) \
			or (line.find( 'format' ) >= 0) \
			or (line.find( 'class' ) >= 0) \
			or (line.find( 'FoamFile' ) >= 0) \
			or (line.find( 'points' ) >= 0) \
			or (line.find( 'object' ) >= 0) \
			or (line.find( '{' ) >= 0) \
			or (line.find( '}' ) >= 0) \
			or (line == '\n'):
		continue
	else:
		line = line.rstrip( '\n' + '' )
		if line == '(' or line == ')':
			pass
		else:
			points.append( line )

filepoints.close( )
points = filter( None, points )

# Überprüfe ob alle Punkte eingelesen wurden
if int( points [ 0 ] ) == (len( points ) - 1):
	print('\n')
	print ('Alle Punkte wurden erfolgreich eingelesen')
else:
	print('\n')
	print('Es ist ein Fehler beim Einlesen der Punkte aufgetreten')

# Lösche den ersten Eintrag, der die Gesamtanzahl der vorhandenen Punkte angibt
del points [ 0 ]

# Points wird zu Pointslist
pointslist = points
del points
del filepoints

# Output der eingelesenen Punkte
print('Output der eingelesenen Punkte')
print(pointslist)

# Einlesen der Punkte-Matrix
points = np.zeros( (len( pointslist )), dtype=[ ('index', np.int64), ('XCoordinate', np.float64), ('YCoordinate', np.float64), ('ZCoordinate', np.float64) ] )

for i in range( len( pointslist ) ):
	points [ i ] [ 'index' ] = np.int64( i )
	zwischenstring = pointslist [ i ]
	zwischenstring = zwischenstring.replace( '(', '' )
	zwischenstring = zwischenstring.replace( ')', '' )
	zwischenstring = zwischenstring.split( )
	points [ i ] [ 'XCoordinate' ] = np.float64( zwischenstring [ 0 ] )
	points [ i ] [ 'YCoordinate' ] = np.float64( zwischenstring [ 1 ] )
	points [ i ] [ 'ZCoordinate' ] = np.float64( zwischenstring [ 2 ] )


print('\n')
print('Output der eingelesenen Punkte als Array')
print(points)

pointsmatrix = (np.sort( points, order=('ZCoordinate', 'YCoordinate', 'XCoordinate'), kind='mergesort' ))
#pointsmatrix = np.zeros( dimensionPoints, dtype=[('index', np.int64), ('XCoordinate', np.float64), ('YCoordinate', np.float64), ('ZCoordinate', np.float64)])
pointsmatrix=np.reshape(pointsmatrix, dimensionPoints, order='C')

print('\n')
print('Das sind die Punkte ausgegeben als Matrix')
print(pointsmatrix)
